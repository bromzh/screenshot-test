const playwrightShooter = require('viteshot/shooters/playwright');
const playwright = require('playwright');

const shooterOptions = {
  // Take each screenshot in two different viewports.
  contexts: {
    laptop: {
      viewport: {
        width: 1366,
        height: 768,
      },
    },
    pixel2: playwright.devices['Pixel 2'],
  },
  output: {
    // Put all screenshots in a top-level __screenshots__ directory.
    suffixPath: `__image_snapshots__/${process.platform}`,
  },
};

module.exports = {
  framework: {
    type: 'react',
  },
  shooter: playwrightShooter(playwright.chromium, shooterOptions),
  extra: shooterOptions,
  filePathPattern: '**/*.screenshot.@(js|jsx|tsx|vue|svelte)',
};
