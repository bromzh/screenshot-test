import React, { FC, PropsWithChildren } from 'react';
import styled from 'styled-components';

export interface FancyButtonProps {
  primary?: boolean;
}

const COLOR = '#5b8ad0';

const StyledButton = styled.button<FancyButtonProps>`
  background: ${p => p.primary ? `${COLOR}` : 'white'};
  color: ${p => p.primary ? 'white' : `${COLOR}`};
  font-weight: 700;
  font-size: 1.1em;
  height: 40px;
  border: 2px solid ${COLOR};
  padding: 0 20px;
`;

export const FancyButton: FC<PropsWithChildren<FancyButtonProps>> = props => {
  return <StyledButton primary={props.primary}>{props.children}</StyledButton>;
};