import { FancyButton, FancyButtonProps } from './FancyButton';

export default {
    title: 'FancyButton',
    component: FancyButton,
};

const Template = (args: FancyButtonProps) => <FancyButton {...args} />;

export const Primary = Template.bind({});
(Primary as any).args = {
    primary: true,
    children: 'Primary',
};

export const Default = Template.bind({});
(Default as any).args = {
    children: 'Default',
};