import React from 'react';
import { FancyButton } from './FancyButton';

export const DefaultLabelScreenshot = () => <FancyButton>Default</FancyButton>;

export const HelloLabelScreenshot = () => <FancyButton primary>Primary</FancyButton>;