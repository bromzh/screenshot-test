import { FancyButton } from './FancyButton';
import puppeteer from 'puppeteer';
import { getScreenshots } from '../../../testing/utils';
import each from 'jest-each';
import { readFileSync } from 'fs';

describe.skip('FancyButton via browser', () => {
  let browser: puppeteer.Browser;

  beforeAll(async () => {
    browser = await puppeteer.launch();
  });

  it('screenshot', async () => {
    const page = await browser.newPage();
    await page.goto('http://localhost:3000');
    const image = await page.screenshot();

    expect(image).toMatchImageSnapshot();
  });

  afterAll(async () => {
    await browser.close();
  });
});

describe('FancyButton', () => {
  let shots: string[] = [];
  beforeAll(() => {
    shots = getScreenshots(__dirname);
  });
  it('Should do smth', () => {
    expect(1).toBe(1);
  })
  // each([shots]).it('Match screenshot %s', (shot) => {
  it('Should match screenshot', () => {
    shots.forEach(shot => {
      const image = readFileSync(shot);
      expect(image).toMatchImageSnapshot();
    });
  });
  // });
  // it('should match screenshot', () => {
  //
  // })
});