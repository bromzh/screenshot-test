import React, { FC } from 'react';
import styled from 'styled-components';

const COLOR = '#5b8ad0';

export const StyledInput = styled.input`
  border: 2px solid ${COLOR};
  height: 40px;
  padding: 0 10px;
`;

export interface TextInputProps {
  value?: string;
  setValue?: (value: string) => void;
}

const defaultSetValue = (e: string) => void e;

export const TextInput: FC<TextInputProps> = props => {
  const setter = props.setValue ?? defaultSetValue;
  return <StyledInput value={props.value} onChange={e => setter(e.target.value)}/>;
};