import { readFileSync } from 'fs';

describe('TextInput screenshot', () => {
  it('must match screenshot', () => {
    const laptopImage = readFileSync(__dirname + '/__image_snapshots__/darwin/laptop/TestInput-TestInputScreenshot.png');
    const pixel2Image = readFileSync(__dirname + '/__image_snapshots__/darwin/pixel2/TestInput-TestInputScreenshot.png');

    expect(laptopImage).toMatchImageSnapshot();
    expect(pixel2Image).toMatchImageSnapshot();
  });
});