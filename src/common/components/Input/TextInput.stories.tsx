import { TextInput, TextInputProps } from './TextInput';

export default {
  title: 'TextInput',
  component: TextInput,
};

const Template = (args: TextInputProps) => <TextInput {...args}/>;

export const Empty = Template.bind({});

export const WithValue = Template.bind({});
(WithValue as any).args = {
    value: 'Some text'
}