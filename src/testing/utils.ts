import viteshotConfig from '../../viteshot.config';
import { readdirSync } from 'fs';
import * as path from 'path';

const options = viteshotConfig.extra;
const contexts = Object.keys(options.contexts);
const suffixPath = options.output.suffixPath;

export const getScreenshots = (baseDir: string) => {
  const contextsPaths = contexts.map(c => path.join(baseDir, suffixPath, c));
  const screens = contextsPaths.flatMap(c => readdirSync(c).map(f => path.join(c, f)));
  return screens;
}