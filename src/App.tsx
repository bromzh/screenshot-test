import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { FancyButton } from './common/components/Button/FancyButton';
import { MainLayout } from './common/layouts/MainLayout';
import { TextInput } from './common/components/Input/TextInput';

function App() {
  const [value, setValue] = useState('');
  return (
    <div className="App">
      <header className="App-header">
        <MainLayout>
          <TextInput value={value} setValue={setValue}/>
          <FancyButton>Send</FancyButton>
        </MainLayout>
      </header>
    </div>
  );
}

export default App;
